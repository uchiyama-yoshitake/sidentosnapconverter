﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SidenToSnapConverter.Converter
{
    public class ImageFileNamager
    {
        private int countInsp = 0;
        private int countRef = 0;

        public void CopyImageFiles(string srcDirPath, string dstDirPath, string extension)
        {
            List<string> srcFileList = Directory.EnumerateFiles(srcDirPath, "*." + extension).ToList();
            srcFileList.Sort();

            for (int i = 0; i < srcFileList.Count; i++)
            {
                CopyImage(srcFileList[i], dstDirPath, extension);
            }
        }

        private void CopyImage(string srcFilePath, string dstDirPath, string extension)
        {
            string srcFileName = Path.GetFileName(srcFilePath);
            string snapInspectionID = "000" + srcFileName.Substring(0, 7);
            
            if (srcFileName.Contains("ZoomDown"))
            {
                string dstFilePath1 = Path.Combine(dstDirPath, ("model." + extension));
                string dstFilePath2 = Path.Combine(dstDirPath, (snapInspectionID + "_t." + extension));
                File.Copy(srcFilePath, dstFilePath1, true);
                File.Copy(srcFilePath, dstFilePath2, true);
            }
            else if ((srcFileName.Contains("MeshBlinkInsp")) || (srcFileName.Contains("MeshBlinkRef")))
            {
                string dstFilePath = string.Format("{0}-{1:00000}{2}.{3}",
                    snapInspectionID,
                    srcFileName.Contains("MeshBlinkInsp") ? countInsp++ : countRef++,
                    srcFileName.Contains("MeshBlinkInsp") ? "_t" : "_m",
                    extension);
                File.Copy(srcFilePath, dstFilePath, true);
            }
        }
    }
}
