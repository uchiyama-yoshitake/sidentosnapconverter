﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SidenToSnapConverter.Siden2ndResultData;

namespace SidenToSnapConverter.Converter
{
    public class Converter
    {
        private string srcDirPath;
        private string srcFileName;
        private string resultDirPath;

        public void Convert(string xmlFilePath, string dstDirPath)
        {
            // 出力ディレクトリ作成
            srcDirPath = PathManager.GetSrcDirectoryPath(xmlFilePath);
            srcFileName = PathManager.GetSrcFileName(xmlFilePath);
            resultDirPath = PathManager.CreateResultDirectory(srcFileName, dstDirPath);

            // xml読み込み
            ResultData resultData = ResultDataReader.ReadXmlFile(xmlFilePath);

            // 画像コピー
            ImageFileNamager imageFileNamager = new ImageFileNamager();
            imageFileNamager.CopyImageFiles(srcDirPath, resultDirPath, "png");//extension

            // avrstファイル作成
            ResultConverter resultConverter = new ResultConverter();
            resultConverter.SaveResultSummary(resultData, srcFileName, dstDirPath);

            // model作成
            resultConverter.SaveInspectionProduct(resultData, dstDirPath);

            // result.db作成
        }
    }
}
