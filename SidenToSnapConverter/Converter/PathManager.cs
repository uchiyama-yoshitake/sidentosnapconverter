﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SidenToSnapConverter.Converter
{
    public static class PathManager
    {
        public static string GetSrcDirectoryPath(string filePath)
        {
            return Path.GetDirectoryName(filePath);
        }

        public static string GetSrcFileName(string filePath)
        {
            return Path.GetFileNameWithoutExtension(filePath);
        }

        public static string CreateResultDirectory(string fileName, string dstDirPath)
        {
            if (!Directory.Exists(dstDirPath))
            {
                Directory.CreateDirectory(dstDirPath);
            }

            string resutlDirPath = Path.Combine(dstDirPath, fileName + ".avrstd");
            if (!Directory.Exists(resutlDirPath))
            {
                Directory.CreateDirectory(resutlDirPath);
            }

            return resutlDirPath;
        }
    }
}
