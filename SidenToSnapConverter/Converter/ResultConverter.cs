﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SidenToSnapConverter.Siden2ndResultData;
using Siden3rdInspection.Models;
using System.IO;

namespace SidenToSnapConverter.Converter
{
    public class ResultConverter
    {
        public void SaveResultSummary(ResultData resultData, string srcFileName, string dstDirPath)
        {
            ResultSummary resultSummary = new ResultSummary();

            string dstFilePath = Path.Combine(dstDirPath, srcFileName + ".avrst");
            ResultSummary.Save(dstFilePath, resultSummary, new Version(), "", true);
        }

        public void SaveInspectionProduct(ResultData resultData, string dstDirPath)
        {
            InspectionProduct product = new InspectionProduct() { IsSerializeMasterSource = false };

            string dstFilePath = Path.Combine(dstDirPath, "model.avmdl");
            InspectionProduct.Save(dstFilePath, product);
        }
    }
}
