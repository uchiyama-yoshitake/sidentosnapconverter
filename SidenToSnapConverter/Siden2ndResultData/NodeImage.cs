﻿using System;
using System.Xml.Serialization;

namespace SidenToSnapConverter.Siden2ndResultData
{
    [Serializable]
    public class NodeImage
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.NodeImage";

        //XML IgnoreAttributes                

        // XML Attributes

        // XML Elements

        [XmlElement(ElementName = "width")]
        public int width_ = 0;

        [XmlElement(ElementName = "height")]
        public int height_ = 0;

        [XmlElement(ElementName = "channel")]
        public int channel_ = 0;

        [XmlElement(ElementName = "resolutionRow")]
        public string resolutionRow_ = "";

        [XmlElement(ElementName = "resolutionCol")]
        public string resolutionCol_ = "";

        [XmlElement(ElementName = "saveMode")]
        public SaveMode saveMode_ = new SaveMode();

        [XmlElement(ElementName = "userInfo")]
        public string userInfo_ = null;


        public NodeImage()
        {
        }

    }



    [Serializable]
    public class SaveMode
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.SaveMode";

        //XML IgnoreAttributes                

        // XML Attributes

        // XML Elements



        //画像保存フォーマット
        [XmlElement(ElementName = "imageFormat")]
        public string imageFormat_ = "bmp";

        //原画像保存
        [XmlElement(ElementName = "imageAllEnable")]
        public bool imageAllEnable_ = false;

        //NG画像保存
        [XmlElement(ElementName = "imageErrEnable")]
        public bool imageErrEnable_ = false;

        //NG画像保存(欠陥情報付き)
        [XmlElement(ElementName = "imageErrDumpEnable")]
        public bool imageErrDumpEnable_ = false;

        //NG画像検査領域枠保存
        [XmlElement(ElementName = "imageErrRegionInspEnable")]
        public bool imageErrRegionInspEnable_ = false;

        //NG画像検査領域枠保存(欠陥情報付き)
        [XmlElement(ElementName = "imageErrRegionInspDumpEnable")]
        public bool imageErrRegionInspDumpEnable_ = false;

        //NG原画像切出保存
        [XmlElement(ElementName = "imageErrMeshEnable")]
        public bool imageErrMeshEnable_ = false;

        //NG原画像切出保存(欠陥情報付き)
        [XmlElement(ElementName = "imageErrMeshDumpEnable")]
        public bool imageErrMeshDumpEnable_ = false;

        //NG原画像縮小保存
        [XmlElement(ElementName = "imageErrZoomDownEnable")]
        public bool imageErrZoomDownEnable_ = false;

        //NG原画像縮小サイズ
        [XmlElement(ElementName = "imageErrZoomDown")]
        public int imageErrZoomDown_ = 1;



        public SaveMode()
        {
        }

    }
}
