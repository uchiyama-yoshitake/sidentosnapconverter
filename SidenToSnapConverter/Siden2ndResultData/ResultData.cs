﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SidenToSnapConverter.Siden2ndResultData
{
    [Serializable]
    [XmlRoot(Namespace = "www.navitas.co.jp", ElementName = "ResultData", DataType = "string", IsNullable = true)]
    public class ResultData
    {
        //XML IgnoreAttributes
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.ResultData";

        //DOWA 位置決め失敗 再検査のため全体画像をbmpで保存
        [XmlIgnore]
        public bool judgeAlignFind_ = true;

        //DOWA 寸法計測 エッジ未検出 再検査のため全体画像をbmpで保存
        [XmlIgnore]
        public bool judgeMeasureFind_ = true;


        //XML Attributes


        //XML Elements

        [XmlElement(ElementName = "nodeType")]
        public string nodeType_ = "Root";

        //現在時刻
        [XmlElement(ElementName = "timeCurrent")]
        public string timeCurrent_ = "";

        //モデル名
        [XmlElement(ElementName = "modelName")]
        public string modelName_ = "";

        //ユーザタイプ
        [XmlElement(ElementName = "userType")]
        public int userType_ = 0;

        //トリガカウント
        [XmlElement(ElementName = "trigCount")]
        public int trigCount_ = 0;

        //ピースカウント
        [XmlElement(ElementName = "pieceCount")]
        public int pieceCount_ = 0;

        //ピース間カウント 
        //OSP ピース行数 後工程で停止するためにPLCがカウントし検査ログに保存
        [XmlElement(ElementName = "pieceRowCount")]
        public int pieceRowCount_ = 0;

        //歯抜け
        [XmlElement(ElementName = "noBladeCount")]
        public int noBladeCount_ = 0;


        //ロケーション番号 
        //OSP ショット脇に印字された文字列をOCRした数値 基準文字列CSVからサーチし頭出し
        //[XmlElement(ElementName = "locationNo")]
        //public int locationNo_ = 0;
        //trigCountがCSVの該当行数にあたるので保存不要

        //エンコーダ距離 mm
        [XmlElement(ElementName = "encoderDistance")]
        public string encoderDistance_ = "";

        //OSP 1F 撮像トリガ受信でエンコーダパルスの生値を出力、CTC印刷抜け、ショット長よりずれてたらVFで警告
        //エンコーダパルス 銀河24bit 最大16777216 int→2,147,483,647で足りる
        [XmlElement(ElementName = "encoderCount")]
        public int encoderCount_ = 0;

        //検査累積数
        [XmlElement(ElementName = "inspCount")]
        public int inspCount_ = 0;

        //OK数
        [XmlElement(ElementName = "okCount")]
        public int okCount_ = 0;

        //NG数
        [XmlElement(ElementName = "ngCount")]
        public int ngCount_ = 0;

        //NGピース数
        [XmlElement(ElementName = "ngPieceCount")]
        public int ngPieceCount_ = 0;

        //検査総合結果
        [XmlElement(ElementName = "judge")]
        public bool judge_ = true;

        //画像処理タイムアウト
        [XmlElement(ElementName = "timeoutProcess")]
        public bool timeoutProcess_ = true;

        //表示タイムアウト
        [XmlElement(ElementName = "timeoutDisplay")]
        public bool timeoutDisplay_ = true;

        //撮像時間
        [XmlElement(ElementName = "timeExecGrab")]
        public string timeExecGrab_ = "";

        //画像処理時間
        [XmlElement(ElementName = "timeExecProcess")]
        public string timeExecProcess_ = "";

        //表示時間
        [XmlElement(ElementName = "timeExecDisplay")]
        public string timeExecDisplay_ = "";

        //保存時間(XML保存を画像保存の後にする) //結局XML自体を保存する時間を記録できないのでやめる
        [XmlElement(ElementName = "timeExecSaveImage")]
        public string timeExecSaveImage_ = "";


        //ワーク情報、帳票や結果ディレクトリ作成などに利用
        [XmlElement(ElementName = "workInfoName01")]
        public string workInfoName01_ = "";
        [XmlElement(ElementName = "workInfoName02")]
        public string workInfoName02_ = "";
        [XmlElement(ElementName = "workInfoName03")]
        public string workInfoName03_ = "";
        [XmlElement(ElementName = "workInfoName04")]
        public string workInfoName04_ = "";
        [XmlElement(ElementName = "workInfoName05")]
        public string workInfoName05_ = "";
        [XmlElement(ElementName = "workInfoName06")]
        public string workInfoName06_ = "";
        [XmlElement(ElementName = "workInfoName07")]
        public string workInfoName07_ = "";
        [XmlElement(ElementName = "workInfoName08")]
        public string workInfoName08_ = "";
        [XmlElement(ElementName = "workInfoName09")]
        public string workInfoName09_ = "";
        [XmlElement(ElementName = "workInfoName10")]
        public string workInfoName10_ = "";

        [XmlElement(ElementName = "workInfo01")]
        public string workInfo01_ = "";
        [XmlElement(ElementName = "workInfo02")]
        public string workInfo02_ = "";
        [XmlElement(ElementName = "workInfo03")]
        public string workInfo03_ = "";
        [XmlElement(ElementName = "workInfo04")]
        public string workInfo04_ = "";
        [XmlElement(ElementName = "workInfo05")]
        public string workInfo05_ = "";
        [XmlElement(ElementName = "workInfo06")]
        public string workInfo06_ = "";
        [XmlElement(ElementName = "workInfo07")]
        public string workInfo07_ = "";
        [XmlElement(ElementName = "workInfo08")]
        public string workInfo08_ = "";
        [XmlElement(ElementName = "workInfo09")]
        public string workInfo09_ = "";
        [XmlElement(ElementName = "workInfo10")]
        public string workInfo10_ = "";


        //NodeTree
        [XmlArray("nodes"), XmlArrayItem("node", typeof(FrameNode))]
        public List<FrameNode> frameNodes_ = new List<FrameNode>();



        public ResultData()
        {
        }


        //public Object Clone()
        //{
        //    return MemberwiseClone();
        //}


        //public ResultData DeepCopy()
        //{
        //    object obj = MemberwiseClone();
        //    return (ResultData)obj;
        //}

    }
}
