﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SidenToSnapConverter.Siden2ndResultData
{
    [Serializable]
    public class FrameNode
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.FrameNode";

        // XML Attributes

        // XML Elements
        [XmlElement(ElementName = "nodeType")]
        //public string nodeType_ = "";
        public string nodeType_ = null;

        [XmlElement(ElementName = "name")]
        //public string name_ = "";
        public string name_ = null;

        //[XmlElement(ElementName = "enable")]
        //public bool enable_ = true;

        [XmlElement(ElementName = "judge")] //NodeImageにjudgeが付くので各Nodeにて持つ
        //public bool judge_ = false;
        public bool judge_ = true;

        [XmlElement(ElementName = "timeExec")]
        public string timeExec_ = null;

        [XmlElement(ElementName = "userInfo")]
        public string userInfo_ = null;

        [XmlElement(ElementName = "info")]
        public string info_ = null;


        //+++++

        //NodeImageがframeを保持、framNodeのTree構造はNodeImage以下から
        [XmlElement(ElementName = "nodeImage")]
        public NodeImage nodeImage_ = null;

        [XmlElement(ElementName = "nodeAlignN")]
        public NodeAlignN nodeAlignN_ = null;

        [XmlElement(ElementName = "nodeDiff")]
        public NodeDiff nodeDiff_ = null;

        //[XmlElement(ElementName = "nodeFlex")]
        //public NodeFlex nodeFlex_ = null;

        //[XmlElement(ElementName = "nodeThreshold")]
        //public NodeThreshold nodeThreshold_ = null;

        //[XmlElement(ElementName = "nodeCode1d")]
        //public NodeCode1d nodeCode1d_ = null;

        //[XmlElement(ElementName = "nodeCode2d")]
        //public NodeCode2d nodeCode2d_ = null;

        //[XmlElement(ElementName = "nodeMeasure")]
        //public NodeMeasure nodeMeasure_ = null;

        //[XmlElement(ElementName = "nodeOcr")]
        //public NodeOcr nodeOcr_ = null;

        //[XmlElement(ElementName = "nodePattern")]
        //public NodePattern nodePattern_ = null;

        //[XmlElement(ElementName = "nodeCalculate")]
        //public NodeCalculate nodeCalculate_ = null;

        //Nodeの連結
        [XmlArray("nodes"), XmlArrayItem("node", typeof(FrameNode))]
        public List<FrameNode> frameNodes_ = new List<FrameNode>();


        public FrameNode()
        {
        }

    }
}
