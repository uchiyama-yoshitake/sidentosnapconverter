﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SidenToSnapConverter.Siden2ndResultData
{
    [Serializable]
    public class NodeAlignN
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.NodeAlignN";

        // XML Attributes

        // XML Elements

        //全体判定 N点位置決め
        //[XmlElement(ElementName = "judge")]
        //public bool judge_ = true;

        [XmlArray("aligns"), XmlArrayItem("align", typeof(Align))]
        public List<Align> aligns_ = new List<Align>();

        [XmlElement(ElementName = "userInfo")]
        public string userInfo_ = null;

        [XmlElement(ElementName = "info")]
        public string info_ = null;


        public NodeAlignN()
        {
        }

    }



    [Serializable]
    public class Align
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.Align";

        //XML IgnoreAttributes

        //XML Attributes

        //個別判定
        [XmlAttribute(DataType = "boolean", AttributeName = "judge")]
        public bool judge_ = false;

        [XmlAttribute(DataType = "string", AttributeName = "rowInsp")]
        public string rowInsp_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "colInsp")]
        public string colInsp_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "rowRef")]
        public string rowRef_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "colRef")]
        public string colRef_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "angle")]
        public string angle_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "scale")]
        public string scale_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "type")]
        public string type_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "info")]
        public string info_ = null;

        //XML Elements


        public Align()
        {
        }
    }
}
