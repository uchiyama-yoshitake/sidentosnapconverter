﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SidenToSnapConverter.Siden2ndResultData
{
    [Serializable]
    public class NodeDiff
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.NodeDiff";

        //private Control control_ = null;
        //private NodeUnit.Node node_ = null;

        //XML IgnoreAttributes
        //[XmlIgnore]
        //public ProcessDiff process_ = null;


        // XML Attributes


        // XML Elements

        //[XmlElement(ElementName = "judge")]
        //public bool judge_ = true;

        [XmlArray("diffs"), XmlArrayItem("diff", typeof(Diff))]
        public List<Diff> diffs_ = new List<Diff>();

        [XmlElement(ElementName = "userInfo")]
        public string userInfo_ = null;

        [XmlElement(ElementName = "info")]
        public string info_ = null;



        public NodeDiff()
        {
        }
    }



    [Serializable]
    public class Diff
    {
        private const string className_ = "Siden2nd.NodeUnit.NodeImage.Frame.Diff";

        //XML IgnoreAttributes

        // XML Attributes
        [XmlAttribute(DataType = "string", AttributeName = "rowInsp")]
        public string rowInsp_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "colInsp")]
        public string colInsp_ = null;

        //Viewerのブリンク表示の参照画像の欠陥位置
        [XmlAttribute(DataType = "string", AttributeName = "rowRef")]
        public string rowRef_ = null;

        [XmlAttribute(DataType = "string", AttributeName = "colRef")]
        public string colRef_ = null;

        [XmlAttribute(DataType = "int", AttributeName = "area")]
        public int area_ = -1;


        //東レ詳細形状数値 通常は記録されない→intやdoubleではなくstring=nullでxmlに記載されないようにする
        //とりあえず縦と横のみ、あとで必要あれば追加
        //[XmlAttribute(DataType = "string", AttributeName = "height")]
        //public string height_ = null;
        //[XmlAttribute(DataType = "string", AttributeName = "width")]
        //public string width_ = null;

        //20120112東レ
        //東レ詳細形状数値 通常は記録されない→intやdoubleではなくstring=nullでxmlに記載されないようにする
        //とりあえず縦と横のみ、あとで必要あれば追加
        [XmlAttribute(DataType = "string", AttributeName = "height")]
        public string height_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "width")]
        public string width_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "circularity")]
        public string circularity_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "compactness")]
        public string compactness_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "convexity")]
        public string convexity_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "contlength")]
        public string contlength_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "anisometry")]
        public string anisometry_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "ra")]
        public string ra_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "rb")]
        public string rb_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "phi")]
        public string phi_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "hole")]
        public string hole_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "areaRatio")]
        public string areaRatio_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "grayMin")]
        public string grayMin_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "grayMax")]
        public string grayMax_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "grayMean")]
        public string grayMean_ = null;
        [XmlAttribute(DataType = "string", AttributeName = "grayDeviation")]
        public string grayDeviation_ = null;



        //比較検査パラメータのインデックス
        [XmlAttribute(DataType = "int", AttributeName = "param")]
        public int param_ = -1;

        [XmlAttribute(DataType = "string", AttributeName = "name")]
        public string name_ = null;

        //絶対値、明、暗
        [XmlAttribute(DataType = "string", AttributeName = "diffType")]
        public string diffType_ = null;

        [XmlElement(ElementName = "info")]
        public string info_ = null;

        //XML Elements

        public Diff()
        {
        }
    }
}
