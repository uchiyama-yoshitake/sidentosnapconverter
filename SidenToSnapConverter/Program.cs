﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SidenToSnapConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    Console.WriteLine("USAGE : SidenToSnapConverter.exe XmlFilePath [DstDirectory]");
                    return;
                }

                List<string> argList = GetArgList(args);

                Converter.Converter converter = new Converter.Converter();
                converter.Convert(argList[0], argList[1]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static List<string> GetArgList(string[] args)
        {
            FileInfo xmlFile = new FileInfo(args[0]);
            string dstDirPath;
            if (args.Length == 1)
            {
                dstDirPath = xmlFile.DirectoryName;
            }
            else
            {
                dstDirPath = args[1];
            }

            List<string> argList = new List<string>();
            argList.Add(xmlFile.FullName);
            argList.Add(dstDirPath);
            return argList;
        }
    }
}
